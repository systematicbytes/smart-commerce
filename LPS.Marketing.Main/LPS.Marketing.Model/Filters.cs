﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPS.Marketing.Model
{
    public class Filters
    {
        public string vechiels { get; set; }

        public string realEstate { get; set; }

        public string make { get; set; }

        public string model { get; set; }

        public string priseFrom { get; set; }

        public string priseTo { get; set; }

        public string yearFrom { get; set; }

        public string yearTo { get; set; }

        public string mileageFrom { get; set; }

        public string mileageTo { get; set; }

        public string fuleType { get; set; }

        public string sourtOrder { get; set; }

        public string furnished { get; set; }

        public string rooms { get; set; }

        public string areaFrom { get; set; }

        public string areaTo { get; set; }

        public string job { get; set; }

        public string position { get; set; }

        public string salaryFrom { get; set; }

        public string salaryTo { get; set; }
    }

    public class CarsBrands
    {
        public string Value { get; set; }

        public string Name { get; set; }
    }

    public class FuleType
    {
        public string Value { get; set; }

        public string Name { get; set; }
    }

    public class SourtOrder
    {
        public string Value { get; set; }

        public string Name { get; set; }
    }
}
