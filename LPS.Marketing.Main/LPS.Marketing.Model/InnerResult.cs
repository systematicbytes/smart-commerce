﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPS.Marketing.Model
{
    public class InnerResult
    {
        

        public int id { get; set; }

        //Amount
        public string c000_value_numbers { get; set; }

        //Amount With RS
        public string c000_value { get; set; }

        //Link
        public string thumbvtoprel_link { get; set; }

        //Images
        public string thumbvtoprel_image { get; set; }

        //Link With Hash
        public string linkwithhash_link { get; set; }

        // Label
        public string breadcrumb_value { get; set; }

        // City
        public string breadcrumbtext_value { get; set; }

        //breadcrumb_flag
        public string breadcrumb_flag { get; set; }

        #region Vechiles

        //Honda Label
        public string carshonda_label { get; set; }

        //Honda City
        public string carshonda_value { get; set; }

        //Label
        public string carshyundai_label { get; set; }

        //Hyundai City
        public string carshyundai_value { get; set; }

        //Daihatsu Label
        public string carsdaihatsu_label { get; set; }

        //Daihatsu City
        public string carsdaihatsu_value { get; set; }

        //Suzaki Label
        public string carssuzuki_label { get; set; }

        //Suzuki City
        public string carssuzuki_value { get; set; }

        //Toyota Label
        public string carstoyota_label { get; set; }

        //Toyota City
        public string carstoyota_value { get; set; }

        //Other Car Label
        public string carsother_label { get; set; }

        //Other Car City
        public string carsother_value { get; set; }

        #endregion

        #region RealEstate

        //space_image
        public string space_image { get; set; }

        //space_link
        public string space_link { get; set; }

        //housesrent_label
        public string housesrent_label { get; set; }

        //housesrent_value
        public string housesrent_value { get; set; }

        //housessale_label
        public string housessale_label { get; set; }

        //housessale_value
        public string housessale_value { get; set; }

        //landplots_label
        public string landplots_label { get; set; }

        //landplots_value
        public string landplots_value { get; set; }

        #endregion

        #region Jobs

        //nopicture_label
        public string nopicture_label { get; set; }

        //jobsit_label
        public string jobsit_label { get; set; }

        //jobsit_value
        public string jobsit_value { get; set; }

        //jobsonline_label
        public string jobsonline_label { get; set; }

        //jobsonline_value
        public string jobsonline_value { get; set; }

        //jobs_label
        public string jobs_label { get; set; }

        //jobs_value
        public string jobs_value { get; set; }

        //jobssales_label
        public string jobssales_label { get; set; }

        //jobssales_value
        public string jobssales_value { get; set; }

        //jobsother_label
        public string jobsother_label { get; set; }

        //jobsother_value
        public string jobsother_value { get; set; }

        #endregion

        #region  //PakWheels

        //genericgreen_number
        public string genericgreen_number { get; set; }

        //lazypic_image
        public string lazypic_image { get; set; }

        //carnamead_link
        public string carnamead_link { get; set; }

        //link
        public string link { get; set; }

        //genericwhite_value
        public string genericwhite_value { get; set; }

        //unstyledad_value_4
        public string unstyledad_value_4 { get; set; }

        //genericwhite_number
        public string genericwhite_number { get; set; }
        
        #endregion
    }
}
