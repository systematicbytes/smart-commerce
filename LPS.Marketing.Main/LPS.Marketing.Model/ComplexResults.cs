﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPS.Marketing.Model
{
    public class ComplexResults
    {
        public List<InnerResult> results { get; set; }
    }
}
