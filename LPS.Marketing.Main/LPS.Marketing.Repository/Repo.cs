﻿using LPS.Marketing.Model;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPS.Marketing.Repository
{
    public static class Repo
    {
        public static IEnumerable<InnerResult> GetResults(string url)
        {
            var client = new RestClient("https://api.import.io/store/connector/_magic?");
            var request = new RestRequest(Method.GET);
            request.AddParameter("url", url);
            request.AddParameter("_apikey", "01311d98a091427fbf8ad02d64066b2d8a2057c07a393dcf6634a5e1382e98990e9e9ce68bbce4ccf874df11c737ebc5dd01f19b94cf1edeb6cbb73b0d164ed6c7a9128e5960b0998cff83e3e2e214da");
            var value = client.Execute<List<Results>>(request).Data;

            var result = (from val in value
                          from val2 in val.tables
                          from val3 in val2.results
                          select val3).ToList();

            return result;
        }
        
        public static string GetApiUrlForVechiles(Model.Filters filters)
        {
            var url = "http://www.olx.com.pk/";
            var reqFilters = GetVechilesFilters(filters);

            if (reqFilters != "")
            {
                url = url + filters.vechiels + "/?" + reqFilters.Substring(1);
            }
            else
            {
                url = url + filters.vechiels;
            }

            return url;
        }

        public static string GetApiUrlForRealestate(Model.Filters filters)
        {
            var url = "http://www.olx.com.pk/";
            var reqFilters = GetRealEstateFilters(filters);

            if (reqFilters != "")
            {
                url = url + filters.realEstate + "/?" + reqFilters.Substring(1);
            }
            else
            {
                url = url + filters.realEstate;
            }

            return url;
        }
        
        public static string GetVechilesFilters(Model.Filters filters)
        {
            return GetPriceFilter(filters.priseFrom, filters.priseTo) + GetYearFilters(filters.yearFrom, filters.yearTo) + GetMileageFilters(filters.mileageFrom, filters.mileageTo) + GetFuleType(filters.fuleType) + GetSortOrder(filters.sourtOrder);
        }

        public static string GetRealEstateFilters(Model.Filters filters)
        {
            return GetPriceFilter(filters.priseFrom, filters.priseTo) + IsFurnnished(filters.furnished) + GetRooms(filters.rooms) + GetArea(filters.areaFrom, filters.areaTo) + GetSortOrder(filters.sourtOrder);
        }

        public static string GetPriceFilter(string priseFrom, string priseTo)
        {
            string filters = null;
            if (priseFrom == null && priseTo == null)
            {
                filters = null;
            }
            else if (priseFrom != null && priseTo != null)
            {
                filters = "&search%5Bfilter_float_price%3Afrom%5D=" + priseFrom + "&search%5Bfilter_float_price%3Ato%5D=" + priseTo;
            }
            else if (priseFrom != null && priseTo == null)
            {
                filters = "&search%5Bfilter_float_price%3Afrom%5D=" + priseFrom;
            }
            else if (priseFrom == null && priseTo != null)
            {
                filters = "&search%5Bfilter_float_price%3Ato%5D=" + priseTo;
            }
            return filters;
        }

        public static string GetYearFilters(string yearFrom, string yearTo)
        {
            string year = null;
            if (yearFrom == null && yearTo == null)
            {
                year = null;
            }
            else if (yearFrom != null && yearTo != null)
            {
                year = "&search%5Bfilter_float_year%3Afrom%5D=" + yearFrom + "&search%5Bfilter_float_year%3Ato%5D=" + yearTo;
            }
            else if (yearFrom != null && yearTo == null)
            {
                year = "&search%5Bfilter_float_year%3Afrom%5D=" + yearFrom;
            }
            else if (yearFrom == null && yearTo != null)
            {
                year = "&search%5Bfilter_float_year%3Ato%5D=" + yearTo;
            }
            return year;
        }

        public static string GetMileageFilters(string mileageFrom, string mileageTo)
        {
            string mileage = null;
            if (mileageFrom == null && mileageTo == null)
            {
                mileage = null;
            }
            else if (mileageFrom != null && mileageTo != null)
            {
                mileage = "&search%5Bfilter_float_mileage%3Afrom%5D=" + mileageFrom + "&search%5Bfilter_float_mileage%3Ato%5D=" + mileageTo;
            }
            else if (mileageFrom != null && mileageTo == null)
            {
                mileage = "&search%5Bfilter_float_mileage%3Afrom%5D=" + mileageFrom;
            }
            else if (mileageFrom == null && mileageTo != null)
            {
                mileage = "&search%5Bfilter_float_mileage%3Ato%5D=" + mileageTo;
            }
            return mileage;
        }

        public static string GetFuleType(string fuleType)
        {
            string fule = null;
            if (fuleType == null)
            {
                fule = null;
            }
            else if (fuleType != null)
            {
                fule = "&search%5Bfilter_enum_petrol%5D%5B0%5D=" + fuleType;
            }

            return fule;
        }

        public static string GetSortOrder(string sourtOrder)
        {
            string sortingFilter = null;
            if (sourtOrder == null)
            {
                sortingFilter = null;
            }
            else if (sourtOrder == "MostRecent")
            {
                sortingFilter = "&search%5Border%5D=created_at%3Adesc";
            }
            else if (sourtOrder != null)
            {
                sortingFilter = "&search%5Border%5D=filter_float_price%3" + sourtOrder;
            }

            return sortingFilter;
        }

        public static string GetArea(string areaFrom, string areaTo)
        {
            string area = null;
            if (areaFrom == null && areaTo == null)
            {
                area = null;
            }
            else if (areaFrom != null && areaTo != null)
            {
                area = "&search%5Bfilter_float_ft%3Afrom%5D=" + areaFrom + "&search%5Bfilter_float_ft%3Ato%5D=" + areaTo;
            }
            else if (areaFrom != null && areaTo == null)
            {
                area = "&search%5Bfilter_float_ft%3Afrom%5D=" + areaFrom;
            }
            else if (areaFrom == null && areaTo != null)
            {
                area = "&search%5Bfilter_float_ft%3Ato%5D=" + areaTo;
            }

            return area;
        }

        public static string GetRooms(string rooms)
        {
            string roomFilter = null;
            if (rooms == null)
            {
                roomFilter = null;
            }
            else if (rooms != null)
            {
                roomFilter = "&search%5Bfilter_enum_rooms%5D%5B0%5D=" + rooms;
            }

            return roomFilter;
        }

        public static string IsFurnnished(string furnished)
        {
            string furnishedFilters = null;
            if (furnished == null)
            {
                furnishedFilters = null;
            }
            else if (furnished != null)
            {
                furnishedFilters = "&search%5Bfilter_enum_furnished%5D%5B0%5D=" + furnished;
            }

            return furnishedFilters;
        }

        public static string GetApiUrlForPakWheels(Model.Filters filters)
        {
            return "http://www.pakwheels.com/used-cars/search/-" + filters.model + GetPakWheelFilters(filters);
        }

        public static string GetPakWheelFilters(Model.Filters filters)
        {
            return GetPakWheelPrise(filters.priseFrom, filters.priseTo) + GetPakWheelYear(filters.yearFrom, filters.yearTo) + GetPakWheelMileage(filters.mileageFrom, filters.mileageTo) + GetEngineType(filters.fuleType);
        }

        public static string GetPakWheelPrise(string priseFrom, string priseTo)
        {
            string filters = null;
            if ((priseFrom == null && priseTo == null))
            {
                filters = null;
            }
            else if (priseFrom != null && priseTo != null)
            {
                filters = "/pr_" + priseFrom + "_" + priseTo;
            }
            else if (priseFrom != null && priseTo == null)
            {
                filters = "/pr_" + priseFrom + "_More";
            }
            else if (priseFrom == null && priseTo != null)
            {
                filters = "/pr_Less_" + priseTo;
            }
            return filters;
        }

        public static string GetPakWheelYear(string yearFrom, string yearTo)
        {
            string filters = null;
            if (yearFrom == null && yearTo == null)
            {
                filters = null;
            }
            else if (yearFrom != null && yearTo != null)
            {
                filters = "/yr_" + yearFrom + "_" + yearTo;
            }
            else if (yearFrom != null && yearTo == null)
            {
                filters = "/yr_" + yearFrom + "_More";
            }
            else if (yearFrom == null && yearTo != null)
            {
                filters = "/yr_Less_" + yearTo;
            }
            return filters;
        }

        public static string GetPakWheelMileage(string mileageFrom, string mileageTo)
        {
            string filters = null;
            if (mileageFrom == null && mileageTo == null)
            {
                filters = null;
            }
            else if (mileageFrom != null && mileageTo != null)
            {
                filters = "/ml_" + mileageFrom + "_" + mileageTo;
            }
            else if (mileageFrom != null && mileageTo == null)
            {
                filters = "/ml_" + mileageFrom + "_More";
            }
            else if (mileageFrom == null && mileageTo != null)
            {
                filters = "/ml_Less_" + mileageTo;
            }
            return filters;
        }

        public static string GetEngineType(string fuleType)
        {
            string fule = null;
            if (fuleType == null)
            {
                fule = null;
            }
            else if (fuleType != null)
            {
                fule = "/eg_" + fuleType;
            }
            return fule;
        }

        public static string GetApiUrlForJobs(Model.Filters filters)
        {

            return "http://www.olx.com.pk/" + filters.job + GetJobsFilters(filters.position, filters.salaryFrom, filters.salaryTo);
        }

        public static string GetJobsFilters(string positionType, string salaryFrom, string salaryTo)
        {
            string filters = null;
            if (positionType == null && salaryFrom == null && salaryTo == null)
            {
                filters = null;
            }
            else if (positionType != null && salaryFrom == null && salaryTo == null)
            {
                filters = "/?search%5Bfilter_enum_job_type%5D%5B0%5D=" + positionType;
            }
            else if (positionType == null && salaryFrom != null && salaryTo != null)
            {
                filters = "/?search%5Bfilter_float_salary%3Afrom%5D=" + salaryFrom + "&search%5Bfilter_float_salary%3Ato%5D=" + salaryTo;
            }
            else if (positionType == null && salaryFrom != null && salaryTo == null)
            {
                filters = "/?search%5Bfilter_float_salary%3Afrom%5D=" + salaryFrom;
            }
            else if (positionType == null && salaryFrom == null && salaryTo != null)
            {
                filters = "/?search%5Bfilter_float_salary%3Ato%5D=" + salaryTo;
            }
            else if (positionType != null && salaryFrom != null && salaryTo != null)
            {
                filters = "/?search%5Bfilter_enum_job_type%5D%5B0%5D=" + positionType + "&search%5Bfilter_float_salary%3Afrom%5D=" + salaryFrom + "&search%5Bfilter_float_salary%3Ato%5D=" + salaryTo;
            }
            else if (positionType != null && salaryFrom != null && salaryTo == null)
            {
                filters = "customer-service/?search%5Bfilter_enum_job_type%5D%5B0%5D=" + positionType + "&search%5Bfilter_float_salary%3Afrom%5D=" + salaryFrom;
            }
            else if (positionType != null && salaryFrom == null && salaryTo != null)
            {
                filters = "customer-service/?search%5Bfilter_enum_job_type%5D%5B0%5D=" + positionType + "&search%5Bfilter_float_salary%3Ato%5D=" + salaryTo;
            }

            return filters;
        }

        //Filters

        public static List<CarsBrands> GetOlxCarBrands()
        {
            List <CarsBrands> carsBrands = new List<CarsBrands>();
            carsBrands.Add(new CarsBrands() { Value = "cars/", Name = "All" });
            carsBrands.Add(new CarsBrands() { Value = "cars-honda/", Name = "Honda" });
            carsBrands.Add(new CarsBrands() { Value = "hyundai/", Name = "Hyundai" });
            carsBrands.Add(new CarsBrands() { Value = "cars/", Name = "All" });
            carsBrands.Add(new CarsBrands() { Value = "cars-honda/", Name = "Honda" });
            carsBrands.Add(new CarsBrands() { Value = "hyundai/", Name = "Hyundai" });
            carsBrands.Add(new CarsBrands() { Value = "maruti-suzuki/", Name = "Suzuki" });
            carsBrands.Add(new CarsBrands() { Value = "toyota/", Name = "Toyota" });
            carsBrands.Add(new CarsBrands() { Value = "cars-other/", Name = "Other" });

            return carsBrands;
        }

        public static List<CarsBrands> GetPwCarBrands()
        {
            List<CarsBrands> carsBrands = new List<CarsBrands>();
            carsBrands.Add(new CarsBrands() { Value = "cars/", Name = "All" });
            carsBrands.Add(new CarsBrands() { Value = "cars-honda/", Name = "Honda" });
            carsBrands.Add(new CarsBrands() { Value = "hyundai/", Name = "Hyundai" });
            carsBrands.Add(new CarsBrands() { Value = "cars/", Name = "All" });
            carsBrands.Add(new CarsBrands() { Value = "cars-honda/", Name = "Honda" });
            carsBrands.Add(new CarsBrands() { Value = "hyundai/", Name = "Hyundai" });
            carsBrands.Add(new CarsBrands() { Value = "maruti-suzuki/", Name = "Suzuki" });
            carsBrands.Add(new CarsBrands() { Value = "toyota/", Name = "Toyota" });
            carsBrands.Add(new CarsBrands() { Value = "cars-other/", Name = "Other" });

            return carsBrands;
        }

        public static List<FuleType> GetFuleTypeList()
        {
            List <FuleType> fuleTypeList = new List<FuleType>();
            fuleTypeList.Add(new FuleType() { Value = "petrol", Name = "Petrol" });
            fuleTypeList.Add(new FuleType() { Value = "diesel", Name = "Diesel" });
            fuleTypeList.Add(new FuleType() { Value = "lpg", Name = "LPG" });
            fuleTypeList.Add(new FuleType() { Value = "cng", Name = "CNG & Hybrid" });

            return fuleTypeList;
        }
    }
}
