﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LPS.Marketing.Main.Startup))]
namespace LPS.Marketing.Main
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
