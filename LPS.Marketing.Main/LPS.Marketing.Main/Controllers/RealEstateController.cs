﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LPS.Marketing.Main.Controllers
{
    public class RealEstateController : Controller
    {
        // GET: RealEstate
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Houses(Model.Filters filters)
        {
            if (filters.realEstate == null || filters.realEstate == "Category")
            {
                filters.realEstate = "houses";
            }
            string url = Repository.Repo.GetApiUrlForRealestate(filters);
            var results = Repository.Repo.GetResults(url);
            return PartialView(results);
        }

        public ActionResult Apartments()
        {
            return View();
        }

        public ActionResult _Appartment(Model.Filters filters)
        {
            if (filters.realEstate == null || filters.realEstate == "Category")
            {
                filters.realEstate = "apartments";
            }
            string url = Repository.Repo.GetApiUrlForRealestate(filters);
            var results = Repository.Repo.GetResults(url);
            return View(results);
        }

        public ActionResult Roommates()
        {
            return View();
        }

        public ActionResult _Roommates(Model.Filters filters)
        {
            filters.realEstate = "pg-roommates";
            string url = Repository.Repo.GetApiUrlForRealestate(filters);
            var results = Repository.Repo.GetResults(url);
            return PartialView(results);
        }

        public ActionResult LandPlots()
        {
            return View();
        }

        public ActionResult _LandPlots(Model.Filters filters)
        {
            if (filters.realEstate == null || filters.realEstate == "Category")
            {
                filters.realEstate = "land-plots";
            }

            string url = Repository.Repo.GetApiUrlForRealestate(filters);
            var results = Repository.Repo.GetResults(url);
            return PartialView(results);
        }

        public ActionResult CommercialSpace()
        {
            return View();
        }

        public ActionResult _CommercialSpace(Model.Filters filters)
        {
            if (filters.realEstate == null || filters.realEstate == "Category")
            {
                filters.realEstate = "commercial-space";
            }

            string url = Repository.Repo.GetApiUrlForRealestate(filters);
            var results = Repository.Repo.GetResults(url);
            return PartialView(results);
        }
        
        public ActionResult GuestHouse()
        {
            return View();
        }

        public ActionResult _GuestHouse(Model.Filters filters)
        {
            filters.realEstate = "guest-houses";

            string url = Repository.Repo.GetApiUrlForRealestate(filters);

            var results = Repository.Repo.GetResults(url);
            
            return View(results);
        }
    }
}