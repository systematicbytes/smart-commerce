﻿using LPS.Marketing.Model;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LPS.Marketing.Main.Controllers
{
    public class VechilesController : Controller
    {
        // GET: Vechiles
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Cars(Model.Filters filters)
        {
            if (filters.vechiels == null)
            {
                filters.vechiels = "cars";
            }
            else if (filters.vechiels == "brand")
            {
                filters.vechiels = "cars";
            }

            string url = Repository.Repo.GetApiUrlForVechiles(filters);

            var result = Repository.Repo.GetResults(url);

            return PartialView(result);
        }

        public ActionResult Commerical()
        {
            return View();
        }

        public ActionResult CommericalVechiles(Model.Filters filters)
        {
            filters.vechiels = "commercial-vehicles";

            string url = Repository.Repo.GetApiUrlForVechiles(filters);
            var results = Repository.Repo.GetResults(url);
            
            return PartialView(results);
        }

        public ActionResult Other()
        {
            return View();
        }
        
        public ActionResult OtherVechiles(Model.Filters filters)
        {
            filters.vechiels = "other-vehicles/";

            string url = Repository.Repo.GetApiUrlForVechiles(filters);
            var results = Repository.Repo.GetResults(url);
            
            return PartialView(results);
        }

        public ActionResult PakWheels()
        {
            return View();
        }

        public ActionResult _PakWheels(Model.Filters filters)
        {
            string url = Repository.Repo.GetApiUrlForPakWheels(filters);
            var results = Repository.Repo.GetResults(url);
            return PartialView(results);
        }
    }
}