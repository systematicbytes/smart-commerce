﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LPS.Marketing.Main.Controllers
{
    public class JobsController : Controller
    {
        // GET: Jobs
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CustomerServices(Model.Filters filters)
        {
            filters.job = "customer-service";

            string url = Repository.Repo.GetApiUrlForJobs(filters);

            var results = Repository.Repo.GetResults(url);
            
            return PartialView(results);
        }

        public ActionResult IT()
        {
            return View();
        }

        public ActionResult _IT(Model.Filters filters)
        {
            filters.job = "it";
            string url = Repository.Repo.GetApiUrlForJobs(filters);
            var results = Repository.Repo.GetResults(url);
            return PartialView(results);
        }

    }
}