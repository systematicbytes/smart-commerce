﻿
$(document).ready(function () {
    var availableTags = [
      "1000",
      "5000",
      "10000",
      "25000",
      "50000",
      "1000000"
    ];

    var salaryRange = [
      "500",
      "1000",
      "2000",
      "5000",
      "10000",
      "20000",
      "50000",
      "1000000"
    ];

    var years = [
      "2010",
      "2011",
      "2012",
      "2013",
      "2014",
      "2015",
      "2016",
      "2017",
      "2018",
      "2019",
      "2020"
    ];

    var mileageRange = [
      "1000",
      "5000",
      "10000",
      "30000",
      "50000",
      "80000"
    ];

    var area = [
      "300",
      "600",
      "900",
      "1250",
      "1750",
      "2250",
      "2500",
      "5000"
    ];

    $("#priseFrom").autocomplete({
        source: availableTags,
        minLength: 0,
        scroll: true
    }).focus(function () {
        $(this).autocomplete("search", "");
    });

    $("#priseTo").autocomplete({
        source: availableTags,
        minLength: 0,
        scroll: true
    }).focus(function () {
        $(this).autocomplete("search", "");
    });

    $("#salaryFrom").autocomplete({
        source: salaryRange,
        minLength: 0,
        scroll: true
    }).focus(function () {
        $(this).autocomplete("search", "");
    });

    $("#salaryTo").autocomplete({
        source: salaryRange,
        minLength: 0,
        scroll: true
    }).focus(function () {
        $(this).autocomplete("search", "");
    });

    $("#yearFrom").autocomplete({
        source: years,
        minLength: 0,
        scroll: true
    }).focus(function () {
        $(this).autocomplete("search", "");
    });

    $("#yearTo").autocomplete({
        source: years,
        minLength: 0,
        scroll: true
    }).focus(function () {
        $(this).autocomplete("search", "");
    });

    $("#mileageFrom").autocomplete({
        source: mileageRange,
        minLength: 0,
        scroll: true
    }).focus(function () {
        $(this).autocomplete("search", "");
    });

    $("#mileageTo").autocomplete({
        source: mileageRange,
        minLength: 0,
        scroll: true
    }).focus(function () {
        $(this).autocomplete("search", "");
    });

    $("#areaFrom").autocomplete({
        source: area,
        minLength: 0,
        scroll: true
    }).focus(function () {
        $(this).autocomplete("search", "");
    });

    $("#areaTo").autocomplete({
        source: area,
        minLength: 0,
        scroll: true
    }).focus(function () {
        $(this).autocomplete("search", "");
    });


    //Prevent Characters
    $("#priseFrom").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            $("#errmsg").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });

    $("#priseTo").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            $("#errmsg").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });

    $("#salaryFrom").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            $("#errmsg").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });

    $("#salaryTo").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            $("#errmsg").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });
});

