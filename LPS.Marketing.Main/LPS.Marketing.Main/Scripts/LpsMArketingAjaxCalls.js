﻿var current_effect = "img";

function GenerateResults() {
    activateLoader();
    var param = {
        model: $('#vechiels').val(),
        priseFrom: $('#priseFrom').val(),
        priseTo: $('#priseTo').val(),
        yearFrom: $('#yearFrom').val(),
        yearTo: $('#yearTo').val(),
        mileageFrom: $('#mileageFrom').val(),
        mileageTo: $('#mileageTo').val(),
        fuelType: $('#fuelType').val(),
    };
    $.ajax(
        {
            type: 'POST',
            url: '@Url.Action("_PakWheels", "Vechiles")',
            dataType: "html",
            data: param,
            success: function (result) {
                deActivateLoader();
                $('#divWrapper').html(result);
            }
        });

}

function activateLoader() {
    run_waitMe(current_effect);
};

function deActivateLoader() {
    $('body').waitMe('hide');
};

function run_waitMe(effect) {
    $('body').waitMe({
        effect: effect,
        text: 'Please wait...',
        bg: 'rgba(255,255,255,0.7)',
        color: '#000',
        sizeW: '',
        sizeH: '',
        source: 'img.svg',
        onClose: function () { }
    });
}